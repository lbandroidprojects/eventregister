package com.example.lukaszb.eventregister1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by lukaszb on 10.01.2018.
 */
public class DBHelper extends SQLiteOpenHelper {
    public  static final String DB_NAME="events.db";
    public static final String TABLE_NAME="events";
    public static final String EVENT_TYPE="type";
    public static final String EVENT_TIME="event_time";
    public DBHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL("create table if not exists "+TABLE_NAME+" ("+
       "_id integer primary key, "+
       EVENT_TYPE+" text not null, "+
       EVENT_TIME+" text not null)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void saveEvent(String type, Date timeStamp){
        ContentValues cv=new ContentValues();
        cv.put(EVENT_TYPE,type);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        cv.put(EVENT_TIME,sdf.format(timeStamp));
        getWritableDatabase().insert(TABLE_NAME,null,cv);
    }

    public Cursor getAllEvents(){
        return getReadableDatabase().rawQuery("select * from "+TABLE_NAME,null);
    }

    public void clearEvents(){
        getWritableDatabase().delete(TABLE_NAME,null,null);
    }
}
