package com.example.lukaszb.eventregister1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private EventReceiver ev;
    private IntentFilter eventFilter;
    private DBHelper helper;
    private EventAdapter eventAdapter;
    private ListView eventListView;
    private Button bClear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        eventFilter=new IntentFilter();
        eventFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        eventFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        ev=new EventReceiver();
        helper=new DBHelper(this);

        eventAdapter=new EventAdapter(this,helper.getAllEvents());
        eventListView=findViewById(R.id.lvEvents);
        eventListView.setAdapter(eventAdapter);

        bClear=findViewById(R.id.bClear);
        bClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder= new AlertDialog.Builder(v.getContext());
                builder.setIcon(android.R.drawable.alert_light_frame).setMessage("Czy chcesz usunąć zdarzenia ?")
                        .setTitle("Potwierdź").setPositiveButton("Tak",new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        helper.clearEvents();
                        eventAdapter.changeCursor(helper.getAllEvents());
                    }
                }).setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(ev,eventFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(ev);
    }

    private class EventReceiver extends BroadcastReceiver{

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
           switch (intent.getAction().toString()) {
               case Intent.ACTION_POWER_CONNECTED:
               helper.saveEvent("Power ON",new Date());
               break;
               case Intent.ACTION_POWER_DISCONNECTED:
                   helper.saveEvent("Power OFF", new Date());

           }
           eventAdapter.changeCursor(helper.getAllEvents());
        }

    }

    private class EventAdapter extends CursorAdapter{

        public EventAdapter(Context context, Cursor c) {
            super(context, c, 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View v=null;
            if (cursor != null)
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_line, parent, false);
            return v;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            TextView tvType=view.findViewById(R.id.tvType);
            TextView tvTime=view.findViewById(R.id.tvTime);
            tvType.setText(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_TYPE)));
            tvTime.setText(cursor.getString(cursor.getColumnIndex(DBHelper.EVENT_TIME)));
        }
    }
}
